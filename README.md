# AgendaManif


## Description

> (copier/coller du texte de [@MissTaire](https://framapiaf.org/@MissTaire@piaille.fr), parce que quand quelqu'un fait quelque chose bien, bah, faut le prendre !)

**Vous ne savez pas quoi faire ce ouiquende, vous voulez savoir où rejoindre une manif, en fonction de dates ou de lieux, rendez-vous sur l'[AgendaManif](https://agendamanif.logaton.fr).**

Concocté avec amour 🥰 par [@jeeynet](https://framapiaf.org/@jeeynet) _(NDA : c'est moi o_O !_), c'est un site qui recense les annonces de manif écolo-fémino-socio-terroristes passées sur Mastodon, avec l'entrée **Lieux** (donc sur carte) et l'entrée **Date** (donc liste des manifs par dates).

Le mode d'emploi est sur le site : https://agendamanif.logaton.fr/

Pour que la carte reste lisible, ça va nous demander un peu de discipline 🥰 collective ⤵️

1 - D'abord, pour que l'agenda reste lisible, vérifiez si possible 🥰 que la manif que vous voulez annoncer n'existe pas déjà dans l'AgendaManif.

Si elle existe déjà, vous pouvez rebooster le message original qui sera donc visible dans AgendaManif, dans la fenêtre contextuelle qui s'ouvre en activant le point sur la carte (en fin de message contextuel : Source)

Rien ne vous empêche de faire votre propre message, bien entendue, mais si possible sans les hashtags (voir ci-après) permettant de l'afficher sur la carte, sinon, ça s'affichera 2 fois. Donc si on est 15 à annoncer avec les bons hashtags, ça affichera 15 points sur la carte.

2 - Pour que la manif s'affiche, 4 conditions :

- Ajouter un hashtag **#AgendaManif** à votre pouet

- Ajouter un hashtag de date : **#jjMois** :  composé du jour (le numéro du jour, avec 2 chiffres) et le nom du mois (exemple: _#01janvier_ )
 
- Ajouter un hashtag **#CodePostal** pour situer sur la carte (exemple : _#93450_ )

- Ajouter le hashtag **#MotCléOfficielDeLaManif** (exemple : _#StopGreenDock_)

3 - Essayer de faire des pouets initiaux sobres et courts pour que les fenêtres contextuelles des points de manif sur la carte restent lisibles.

Exemple : 
https://framapiaf.org/@jeeynet/112455231616143728 :
> #Sarabande
> Convoi du Lot-et-Garonne contre les LGV 
> 
> Plus d'infos sur https://www.lgvnonmerci.fr/sarabande/
> 
> #AgendaManif #26juin #33840 #LGVNonMerci

Quitte à lier d'autres infos dans les pouets suivants si infos complémentaires 

## Installation
Copier le contenu de l'archive dans votre dossier "web". Bon, faut PHP.

## Roadmap
- Un gros nettoyage parce que c'est du quick&dirty de non-dev
- Prendre en compte les hashtags que ce soit en minuscule ou majuscule, avec des accents ou pas
- Trier les publications dont la date serait passéee
- et tellement d'autres choses pour faire les choses bien (genre tout recommencer de zéro :p)

## Contribution
Toute aide, soutien, correction, encouragement sont vivement encouragées !
Toute plainte, remarque quant à la beauté du code, critique sur le pourquoi du comment doivent être envoyées au président de la république actuel.


## Authors and acknowledgment
Un grand merci à [@MissTaire](https://framapiaf.org/@MissTaire@piaille.fr) qui aura eu une bonne idée (retrouver facilement les manifs du moment) que j'ai interprêté à ma manière ; merci à son soutien indéfectible ; merci à son attention permanente pour mes nuits de sommeil !

## License
[WTFPL : Do What The Fuck You Want to Public License](https://fr.wikipedia.org/wiki/WTFPL)


## Project status
Proof Of Concept
